user_email = 'me@casaretail.ai'
user = Spree::User.find_by(email: user_email) || Spree::User.create(email: user_email, password: 'casa1234')

v_one = Spree::Variant.first
v_two = Spree::Variant.second || variant_id_one
v_three = Spree::Variant.third || variant_id_two

orders = [
    {
        items: [{variant: v_one, quantity: 1}]
    },
    {
        items: [{variant: v_two, quantity: 3}, {variant: v_three, quantity: 1}]
    }
]

orders.each do |o|
    order = user.orders.first || Spree::Cart::Create.call(user: user, store: Spree::Store.first, currency: 'INR').value
    o[:items].each do |item|
        Spree::Cart::AddItem.call order: order, variant: item[:variant], quantity: item[:quantity]
    end
end