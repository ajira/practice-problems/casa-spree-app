products = [
    {name: 'Product One', sku: 'CASA001', price: 523, shipping_category_id: 1},
    {name: 'Product Two', sku: 'CASA002', price: 522, shipping_category_id: 1},
    {name: 'Product Tre', sku: 'CASA003', price: 521, shipping_category_id: 1}
]
products.each do |product|
    Spree::Product.create(product.merge({available_on: Time.now}))
end