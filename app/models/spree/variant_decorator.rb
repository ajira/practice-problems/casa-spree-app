module Spree
    module VariantDecorator
        def backorderable?
            true
        end
    end
end

Spree::Variant.prepend Spree::VariantDecorator